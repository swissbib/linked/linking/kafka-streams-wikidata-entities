### Wikidata Normalizer

A metafacture pipeline to normalize wikidata entities. 

The production metamorph config files can be found [here](https://gitlab.com/swissbib/linked/workflows/-/tree/master/id_hub_workflow/wikidata/import/prod/configs).

The morph files used here are only for test purposes and might not be up to date!


