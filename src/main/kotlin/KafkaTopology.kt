/*
 * wikidata-entities transformer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.io.File
import java.nio.charset.Charset
import java.util.*
import java.util.regex.Pattern
import kotlin.system.exitProcess

class KafkaTopology(private val properties: Properties, private val log: Logger) {
    private val to = properties.getProperty("kafka.topic.sink")
    private val itemPattern = Pattern.compile("<http://www\\.w3\\.org/1999/02/22-rdf-syntax-ns#type> <http://wikiba\\.se/ontology#Item>")
    private val humanPattern = Pattern.compile("<http://www\\.wikidata\\.org/prop/direct/P31> <http://www\\.wikidata\\.org/entity/Q5>")
    private val typePattern = Pattern.compile("<http://www\\.wikidata\\.org/prop/direct/P31> <http://www\\.wikidata\\.org/entity/(Q\\d+)>")
    private val csvPath = properties.getProperty("path.csv")
    private val organisationSet = createSet("$csvPath/organisations.csv")
    private val eventSet = createSet("$csvPath/events.csv")
    private val morphPath = properties.getProperty("path.morph")

    private val humanPipe = WikidataMfPipe("$morphPath/humansMorph", log)
    private val organisationPipe = WikidataMfPipe("$morphPath/organisationsMorph", log)
    private val eventsPipe= WikidataMfPipe("$morphPath/eventsMorph", log)
    private val itemsPipe = WikidataMfPipe("$morphPath/itemsMorph", log)

    private fun createSet(filePath: String): Set<String> {
        val set = mutableSetOf<String>()
        val file = File(filePath)
        if (file.isFile) {
            file.readLines(Charset.defaultCharset()).forEach {
                set.add(it.substringAfterLast('/'))
            }
        }
        if (set.isEmpty()) {
            log.error("Could not load instance classes from $filePath. The set is empty.")
            exitProcess(1)
        }
        return set
    }


    fun build(): Topology {

        val builder = StreamsBuilder()

        val branches = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .branch(
                Predicate { _, value -> branchHuman(value.data) },
                Predicate { _, value -> branchOrganisation(value.data) },
                Predicate { _, value -> branchEvents(value.data) },
                Predicate { _, value -> branchItem(value.data) }
            )

        branches[0]
            .mapValues { value -> pipe(value, humanPipe, "elastic.index.persons") }
            .to(to)

        branches[1]
            .mapValues { value -> pipe(value, organisationPipe, "elastic.index.organisations") }
            .to(to)

        branches[2]
            .mapValues { value -> pipe(value, eventsPipe, "elastic.index.events") }
            .to(to)

        branches[3]
            .mapValues { value -> pipe(value, itemsPipe, "elastic.index.items") }
            .to(to)

        return builder.build()
    }

    private fun branchItem(data: String): Boolean {
        return itemPattern.matcher(data).find()
    }

    private fun branchHuman(data: String): Boolean {
        return humanPattern.matcher(data).find()
    }

    private fun branchOrganisation(data: String): Boolean {
        val matcher = typePattern.matcher(data)
        while (matcher.find())
        {
            val id = matcher.group(1)!!
            if (id in organisationSet)
                return true
        }
        return false
    }

    private fun branchEvents(data: String): Boolean {
        val matcher = typePattern.matcher(data)
        while (matcher.find())
        {
            val id = matcher.group(1)!!
            if (id in eventSet)
                return true
        }
        return false
    }

    private fun pipe(value: SbMetadataModel, pipe: WikidataMfPipe, index: String): SbMetadataModel {
        val result = pipe.process(value.data)
        return createSbMetadateModel(result, "${properties.getProperty(index)}-${value.messageDate}")
    }

    private fun createSbMetadateModel(data: String, index: String): SbMetadataModel {
        return SbMetadataModel()
            .setData(data)
            .setEsIndexName(index)
            .setEsBulkAction(EsBulkActions.INDEX)
    }
}
