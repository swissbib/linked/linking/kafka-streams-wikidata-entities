/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.types.EsBulkActions
import java.io.File
import java.nio.charset.Charset


class Tests {
    private val log = LogManager.getLogger()
    private val props = KafkaProperties(log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties)

    private val resourcePath = "src/test/resources"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun testCase1() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q7514",
                SbMetadataModel().setData(readFile("input1.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertEquals("http://www.wikidata.org/entity/Q7514", output.key())
        assertEquals("wikidata-persons-2019-11-08", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output1.json"), output.value().data)
    }
    @Test
    fun testCase2() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q84",
                SbMetadataModel().setData(readFile("input2.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertEquals("http://www.wikidata.org/entity/Q84", output.key())
        assertEquals("wikidata-organisations-2019-11-08", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output2.json"), output.value().data)
    }
    @Test
    fun testCase3() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q683842",
                SbMetadataModel().setData(readFile("input3.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertEquals("http://www.wikidata.org/entity/Q683842", output.key())
        assertEquals("wikidata-organisations-2019-11-08", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output3.json"), output.value().data)
    }
    @Test
    fun testCase4() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q23",
                SbMetadataModel().setData(readFile("input4.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertEquals("http://www.wikidata.org/entity/Q23", output.key())
        assertEquals("wikidata-persons-2019-11-08", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output4.json"), output.value().data)
    }
    @Test
    fun testCase5() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q5159939",
                SbMetadataModel().setData(readFile("input5.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertEquals("http://www.wikidata.org/entity/Q5159939", output.key())
        assertEquals("wikidata-events-2019-11-08", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output5.json"), output.value().data)
    }


    @Test
    fun testCase6() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q42",
                SbMetadataModel().setData(readFile("input6.1.nt")).setMessageDate("2019-11-08")
            )
        )

        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q233",
                SbMetadataModel().setData(readFile("input6.2.nt")).setMessageDate("2019-11-08")
            )
        )
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q514",
                SbMetadataModel().setData(readFile("input6.3.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        val output2 = testDriver.readOutput(
            props.appProperties.getProperty("kafka.topic.sink"),
            StringDeserializer(),
            SbMetadataDeserializer()
        )
        val output3 = testDriver.readOutput(
            props.appProperties.getProperty("kafka.topic.sink"),
            StringDeserializer(),
            SbMetadataDeserializer()
        )

        assertAll("test case 6",
            { assertEquals("http://www.wikidata.org/entity/Q42", output.key()) },
            { assertEquals("wikidata-persons-2019-11-08", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output6.1.json"), output.value().data) },
            { assertEquals("http://www.wikidata.org/entity/Q233", output2.key()) },
            { assertEquals("wikidata-organisations-2019-11-08", output2.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output2.value().esBulkAction) },
            { assertEquals(readFile("output6.2.json"), output2.value().data) },
            { assertEquals("http://www.wikidata.org/entity/Q514", output3.key()) },
            { assertEquals("wikidata-items-2019-11-08", output3.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output3.value().esBulkAction) },
            { assertEquals(readFile("output6.3.json"), output3.value().data) }
        )
    }

    @Test
    fun testCase7() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://www.wikidata.org/entity/Q36378820",
                SbMetadataModel().setData(readFile("input7.nt")).setMessageDate("2019-11-08")
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test item input",
            { assertEquals("http://www.wikidata.org/entity/Q36378820", output.key()) },
            { assertEquals("wikidata-items-2019-11-08", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output7.json"), output.value().data) }
        )




    }

}